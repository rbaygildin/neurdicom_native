package main

import (
	"encoding/json"
	"os"
	"fmt"
	_ "github.com/lib/pq"
	"database/sql"
)

const (
	DRIVERS_IN_MEMORY = "in_memory"
	DRIVERS_POSTGRES  = "postgres"
	DRIVERS_SQLITE    = "sqlite"
	DRIVERS_MYSQL     = "mysql"
)

type Database struct {
	Host     string `json:"host"`
	DB       string `json:"db"`
	User     string `json:"user"`
	Password string `json:"password"`
	Driver   string `json:"driver"`
	SSLMode  string `json:"ssl_mode"`
}

func (db Database) GetConnStr() string {
	return fmt.Sprintf("host=%s user=%s dbname=%s password=%s sslmode=%s",
		db.Host,
		db.User,
		db.DB,
		db.Password,
		db.SSLMode,
	)
}

func (db Database) GetConnection() *sql.DB {
	if db.Driver == "postgres" {
		conn, err := sql.Open("postgres", db.GetConnStr())
		if err != nil {
			panic("Connection to postgres database is not established")
		}
		return conn
	} else {
		panic("Driver " + db.Driver + " is not supported!")
	}
}

type Auth struct {
	Role     string `json:"role"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type Config struct {
	Database Database `json:"database"`
	Auth     []Auth   `json:"auth"`
}

func GetConfig() Config {
	f, e := os.Open("config.json")
	if e != nil {
		panic("Configuration file not found")
	}
	config := Config{}
	json.NewDecoder(f).Decode(&config)
	return config
}
