package main

var studyDaoInstance StudyDao

// Studies
type StudyDao interface {
	Save(study Study) (Study, error)
	Find(id int) (Study, error)
	FindAll() []Study
	Delete(id int) bool
	Exists(id int) bool
}

func GetStudyDao() StudyDao {
	if studyDaoInstance == nil {
		studyDaoInstance = &studyDao{}
	}
	return studyDaoInstance
}

type studyDao struct {
}

func (*studyDao) Save(study Study) (Study, error) {
	panic("implement me")
}

func (*studyDao) Find(id int) (Study, error) {
	panic("implement me")
}

func (*studyDao) FindAll() []Study {
	panic("implement me")
}

func (*studyDao) Delete(id int) bool {
	panic("implement me")
}

func (*studyDao) Exists(id int) bool {
	panic("implement me")
}
