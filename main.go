package main

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"log"
)

func main() {

	r := httprouter.New()
	r.GET("/", DicomImageHandler)
	r.GET("/patients", FindPatientsHandler)
	r.GET("/patients/:id", FindPatientByIdHandler)
	r.POST("/patients", SavePatientHandler)

	log.Fatal(http.ListenAndServe(":8080", r))
}
