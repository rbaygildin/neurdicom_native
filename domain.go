package main

import "time"

type Patient struct {
	Id               int    `json:"id"`
	PatientId        string `json:"patient_id"`
	PatientName      string `json:"patient_name"`
	PatientSex       string `json:"patient_sex"`
	PatientBirthdate time.Time `json:"patient_birthdate"`
}

type Patients []Patient

type Study struct {
	Id                     int    `json:"id"`
	StudyId                string `json:"study_id"`
	StudyInstanceUid       string `json:"study_instance_uid"`
	StudyDate              string `json:"study_date"`
	StudyTime              string `json:"study_time"`
	StudyDescription       string `json:"study_description"`
	AccessionNumber        string `json:"accession_number"`
	ReferringPhysicianName string `json:"referring_physician_name"`
}

type Studie []Study

type Series struct {
	Id                int    `json:"id"`
	SeriesInstanceUID string `json:"series_instance_uid"`
	SeriesDate        string `json:"series_date"`
	SeriesTime        string `json:"series_time"`
	SeriesDescription string `json:"series_description"`
	Modality          string `json:"modality"`
	SeriesNumber      string `json:"series_number"`
	PatientPosition   string `json:"patient_position"`
	BodyPartExamined  string `json:"body_part_examined"`
	ProtocolName      string `json:"protocol_name"`
}

type SeriesList []Series

type Instance struct {
	SOPInstanceUID            string `json:"sop_instance_uid"`
	InstanceNumber            string `json:"instance_number"`
	ColorSpace                string `json:"color_space"`
	PhotometricInterpretation string `json:"photometric_interpretation"`
	BitsAllocated             string `json:"bits_allocated"`
	BitsStored                string `json:"bits_stored"`
	SmallestImagePixelValue   int    `json:"smallest_image_pixel_value"`
	LargestImagePixelValue    int    `json:"largest_image_pixel_value"`
	PixelAspectRation         string `json:"pixel_aspect_ration"`
	PixelSpacing              string `json:"pixel_spacing"`
	Rows                      int    `json:"rows"`
	Columns                   int    `json:"columns"`
	FilePath                  string `json:"file_path"`
}

type Instances []Instance

type Plugin struct {
	Name       string   `json:"name"`
	Version    string   `json:"version"`
	Author     string   `json:"author"`
	Info       string   `json:"info"`
	Docs       string   `json:"docs"`
	Modalities []string `json:"modalities"`
	Tags       []string `json:"tags"`
	Params     string   `json:"params"`
	Result     string   `json:"result"`
	FilePath   string   `json:"file_path"`
}

type Plugins []Plugin

func (plugin *Plugin) GetFile() {

}
