package main

var patientServiceInstance PatientService

type PatientService interface {
	Save(patient Patient) (Patient, error)
	Find(id int) (Patient, error)
	FindAll() []Patient
	Delete(id int) bool
}

func GetPatientService() PatientService {
	if patientServiceInstance == nil {
		patientServiceInstance = &patientService{}
	}
	return patientServiceInstance
}

type patientService struct {
}

func (service *patientService) Save(patient Patient) (Patient, error) {
	return GetPatientDao().Save(patient)
}

func (service *patientService) Find(id int) (Patient, error) {
	return GetPatientDao().Find(id)
}

func (service *patientService) FindAll() []Patient {
	return GetPatientDao().FindAll()
}

func (service *patientService) Delete(id int) bool {
	return GetPatientDao().Delete(id)
}
