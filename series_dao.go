package main

var seriesDaoInstance SeriesDao

// Series
type SeriesDao interface {
	Save(series Series) (Series, error)
	Find(id int) (Series, error)
	FindAll() []Study
	Delete(id int) bool
	Exists(id int) bool
}

func GetSeriesDao() SeriesDao {
	if seriesDaoInstance == nil {
		seriesDaoInstance = &seriesDao{}
	}
	return seriesDaoInstance
}

type seriesDao struct {
}

func (*seriesDao) Delete(id int) bool {
	panic("implement me")
}

func (*seriesDao) Exists(id int) bool {
	panic("implement me")
}

func (*seriesDao) Find(id int) (Series, error) {
	panic("implement me")
}

func (*seriesDao) FindAll() []Study {
	panic("implement me")
}

func (*seriesDao) Save(series Series) (Series, error) {
	panic("implement me")
}
