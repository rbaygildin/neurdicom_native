package main

import (
	"testing"
	"os"
	"time"
	"reflect"
)

const FixturesQ = `
INSERT INTO patients (id, patient_id, patient_name, patient_sex, patient_birthdate)
VALUES 
(1, '1', 'Patient Name 1', 'M', '1956-01-01'),
(2, '2', 'Patient Name 2', 'F', '1956-01-02');
SELECT setval('id_patients_seq', 3)
`

func InitTests(t *testing.T) {
	InitScheme()
	conn := GetConfig().Database.GetConnection()
	_, err := conn.Exec(FixturesQ)
	if err != nil {
		t.Fatal("Can not initialize fixtures")
	}
}

func equals(t *testing.T, a interface{}, b interface{}) {
	if a != b {
		t.Error("One arg: ", a, " does not equal to two arg: ", b)
	}
}

func equalDates(t *testing.T, d1 time.Time, d2 time.Time) {
	res := d1.Day() == d2.Day()
	res = res && (d1.Month() == d2.Month())
	res = res && (d1.Year() == d2.Year())
	if !res {
		t.Error("Date ", d1, " not equal to date ", d2)
	}
}

func isNil(t *testing.T, a interface{}, message ...interface{}) {
	if a != nil {
		t.Error("Object should be null\n", message)
	}
}

func notNil(t *testing.T, a interface{}) {
	if a == nil {
		t.Error("Object should not be null")
	}
}

func isTrue(t *testing.T, a bool, message ...interface{}) {
	if !a {
		t.Error("Object should be truthy\n", message)
	}
}

func isFalse(t *testing.T, a bool, message ...interface{}) {
	if a {
		t.Error("Object should be falsy\n", message)
	}
}

func hasLen(t *testing.T, a interface{}, length int) {
	v := reflect.ValueOf(a)
	if v.Len() != length {
		t.Error("Object ", a, " has not length ", length)
	}
}

func TestPatientDao_Save(t *testing.T) {
	InitTests(t)
	patient := Patient{
		PatientId:        "1",
		PatientName:      "John Doe",
		PatientSex:       "M",
		PatientBirthdate: NewDate(1, 1, 1956),
	}
	patient, err := GetPatientDao().Save(patient)
	isNil(t, err, err)
	patient, err = GetPatientDao().Find(patient.Id)
	isNil(t, err, err)
	equals(t, patient.PatientId, "1")
	equals(t, patient.PatientName, "John Doe")
	equals(t, patient.PatientSex, "M")
	equalDates(t, patient.PatientBirthdate, NewDate(1, 1, 1956))
}

func TestPatientDao_Find(t *testing.T) {
	InitTests(t)
	patient, err := GetPatientDao().Find(1)
	isNil(t, err, err)
	equals(t, patient.PatientId, "1")
	equals(t, patient.PatientName, "Patient Name 1")
	equals(t, patient.PatientSex, "M")
	equalDates(t, patient.PatientBirthdate, NewDate(1, 1, 1956))
}

func TestPatientDao_FindAll(t *testing.T) {
	InitTests(t)
	patients := GetPatientDao().FindAll()
	hasLen(t, patients, 2)
}

func TestPatientDao_Delete(t *testing.T) {
	InitTests(t)
	isTrue(t, GetPatientDao().Delete(1))
	isFalse(t, GetPatientDao().Exists(1))
}

func TestPatientDao_Exists(t *testing.T) {
	InitTests(t)
	isTrue(t, GetPatientDao().Exists(1))
}

func TestMain(m *testing.M) {
	ret := m.Run()
	os.Exit(ret)
}