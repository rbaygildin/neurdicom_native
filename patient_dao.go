package main

import (
	_ "github.com/lib/pq"
)

const SaveQ = `
INSERT INTO patients (patient_id, patient_name, patient_sex, patient_birthdate)
VALUES ($1, $2, $3, $4) RETURNING id;
`

const FindAllQ = `
SELECT id, patient_id, patient_name, patient_sex, patient_birthdate 
FROM patients;
`

const FindOneQ = `
SELECT id, patient_id, patient_name, patient_sex, patient_birthdate 
FROM patients WHERE id = $1;
`
const DeleteQ = `
DeleteQ FROM patients WHERE id = $1;
`

const ExistsQ = `
SELECT exists(SELECT 1 FROM patients WHERE id = $1);
`

var patientDaoInstance PatientDao

type PatientDao interface {
	Save(patient Patient) (Patient, error)
	Find(id int) (Patient, error)
	FindAll() []Patient
	Delete(id int) bool
	Exists(id int) bool
}

func GetPatientDao() PatientDao {
	if patientDaoInstance == nil {
		config := GetConfig()
		if config.Database.Driver == DRIVERS_POSTGRES {
			patientDaoInstance = &patientDao{}
		} else {
			panic("Driver " + config.Database.Driver + " is not supported")
		}
	}
	return patientDaoInstance
}

type patientDao struct {

}

func (dao *patientDao) Save(patient Patient) (Patient, error) {
	conn := GetConfig().Database.GetConnection()
	defer conn.Close()
	var lastInsertedId int
	err := conn.QueryRow(SaveQ, patient.PatientId, patient.PatientName, patient.PatientSex, patient.PatientBirthdate).Scan(&lastInsertedId)
	if err != nil {
		return Patient{}, err
	}
	patient.Id = lastInsertedId
	return patient, nil
}

func (dao *patientDao) Find(id int) (Patient, error) {
	conn := GetConfig().Database.GetConnection()
	defer conn.Close()
	var patient Patient
	err := conn.QueryRow(FindOneQ, id).Scan(
		&patient.Id, &patient.PatientId, &patient.PatientName,
		&patient.PatientSex, &patient.PatientBirthdate,
	)
	if err != nil {
		return Patient{}, err
	}
	return patient, nil
}

func (dao *patientDao) FindAll() []Patient {
	conn := GetConfig().Database.GetConnection()
	defer conn.Close()
	patients := make(Patients, 0)
	rows, err := conn.Query(FindAllQ)
	if err != nil {
		return patients
	}
	for rows.Next() {
		patient := Patient{}
		err := rows.Scan(
			&patient.Id, &patient.PatientId, &patient.PatientName,
			&patient.PatientSex, &patient.PatientBirthdate,
		)
		if err != nil {
			return make(Patients, 0)
		}
		patients = append(patients, patient)
	}
	return patients
}

func (dao *patientDao) Delete(id int) bool {
	conn := GetConfig().Database.GetConnection()
	defer conn.Close()
	stmt, err := conn.Prepare(DeleteQ)
	if err != nil {
		return false
	}
	_, err = stmt.Exec(id)
	if err != nil {
		return false
	}
	return true
}

func (dao *patientDao) Exists(id int) bool {
	var exists bool
	conn := GetConfig().Database.GetConnection()
	defer conn.Close()
	conn.QueryRow(ExistsQ, id).Scan(&exists)
	return exists
}
