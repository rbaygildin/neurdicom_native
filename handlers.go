package main

import (
	"net/http"
	"github.com/julienschmidt/httprouter"
	"image"
	"image/color"
	"image/jpeg"
	"encoding/json"
	"strconv"
)

func FindPatientsHandler(writer http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	patients := GetPatientService().FindAll()
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(patients)
}

func FindPatientByIdHandler(writer http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, _ := strconv.Atoi(ps.ByName("id"))
	patient, e := GetPatientService().Find(int(id))
	if e != nil {
		writer.WriteHeader(http.StatusNotFound)
		json.NewEncoder(writer).Encode(struct {
			Message string `json:"message"`
		}{Message: e.Error()})
		return
	}
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(patient)
}

func SavePatientHandler(writer http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	patient := Patient{}
	json.NewDecoder(r.Body).Decode(&patient)
	patient, _ = GetPatientService().Save(patient)
	writer.Header().Set("Content-Type", "application/json")
	json.NewEncoder(writer).Encode(patient)
}

func DicomImageHandler(writer http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	d, w, h := ExtractPixelData("/Users/macbook/Desktop/brain.dcm")
	img := image.NewRGBA(image.Rectangle{Min: image.Point{X: 0, Y: 0}, Max: image.Point{X: w, Y: h}})
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			intensity := d[x+y*w]
			img.SetRGBA(x, y, color.RGBA{intensity, intensity, intensity, 255})
		}
	}
	writer.Header().Set("Content-Type", "image/jpeg")
	jpeg.Encode(writer, img, nil)
}
