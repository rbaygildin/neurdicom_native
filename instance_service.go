package main

var instanceServiceInstance InstanceService

type InstanceService interface {
	Save(instance Instance) (Instance, error)
	Find(id int) (Instance, error)
	FindAll() []Instance
	Delete(id int) bool
}

func GetInstanceService() InstanceService{
	if instanceServiceInstance == nil{
		instanceServiceInstance = &instanceService{}
	}
	return instanceServiceInstance
}

type instanceService struct {

}

func (*instanceService) Save(instance Instance) (Instance, error) {
	panic("implement me")
}

func (*instanceService) Find(id int) (Instance, error) {
	panic("implement me")
}

func (*instanceService) FindAll() []Instance {
	panic("implement me")
}

func (*instanceService) Delete(id int) bool {
	panic("implement me")
}

