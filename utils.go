package main

import (
	"github.com/grailbio/go-dicom"
	"github.com/grailbio/go-dicom/dicomtag"
	"time"
)

const SchemeQ = `
DROP TABLE IF EXISTS instances;
DROP TABLE IF EXISTS series;
DROP TABLE IF EXISTS studies;
DROP TABLE IF EXISTS patients;
CREATE SEQUENCE id_patients_seq START 1;
CREATE SEQUENCE id_studies_seq START 1;
CREATE SEQUENCE id_series_seq START 1;
CREATE SEQUENCE id_instances_seq START 1;

CREATE TABLE patients
(
  id                INTEGER PRIMARY KEY default nextval('id_patients_seq'),
  patient_name      VARCHAR(100),
  patient_id        VARCHAR(100),
  patient_sex       VARCHAR(10),
  patient_age       VARCHAR(30),
  patient_birthdate DATE
);

CREATE TABLE studies
(
  id                       INTEGER PRIMARY KEY default nextval('id_studies_seq'),
  study_instance_uid       VARCHAR(80) NOT NULL UNIQUE,
  study_id                 VARCHAR(100),
  study_description        VARCHAR(300),
  referring_physician_name VARCHAR(100),
  study_date               DATE,
  patient_id               INTEGER NOT NULL REFERENCES patients ON DELETE CASCADE
);

CREATE TABLE series
(
  id                  INTEGER PRIMARY KEY default nextval('id_series_seq'),
  series_instance_uid VARCHAR(80) NOT NULL UNIQUE,
  protocol_name       VARCHAR(150),
  modality            VARCHAR(80) NOT NULL,
  series_number       VARCHAR(80),
  patient_position    VARCHAR(30) NOT NULL,
  body_part_examined  VARCHAR(50),
  study_id            INTEGER     NOT NULL REFERENCES studies ON DELETE CASCADE 
);

CREATE TABLE instances
(
  id                         INTEGER PRIMARY KEY default nextval('id_instances_seq'),
  sop_instance_uid           VARCHAR(80)  NOT NULL UNIQUE,
  instance_number            INTEGER      NOT NULL,
  rows                       INTEGER      NOT NULL,
  columns                    INTEGER      NOT NULL,
  color_space                VARCHAR(30),
  photometric_interpretation VARCHAR(30),
  smallest_image_pixel_value INTEGER,
  largest_image_pixel_value  INTEGER,
  pixel_aspect_ratio         VARCHAR(30),
  pixel_spacing              VARCHAR(30),
  image                      VARCHAR(100) NOT NULL,
  series_id                  INTEGER      NOT NULL REFERENCES series ON DELETE CASCADE
);
`

func ExtractPixelData(filePath string) ([]byte, int, int) {
	ds, _ := dicom.ReadDataSetFromFile(filePath, dicom.ReadOptions{})
	wE, _ := ds.FindElementByName("Columns")
	hE, _ := ds.FindElementByName("Rows")
	w := int(wE.Value[0].(uint16))
	h := int(hE.Value[0].(uint16))
	elem, _ := ds.FindElementByTag(dicomtag.PixelData)
	data := elem.Value[0].(dicom.PixelDataInfo)
	frame := data.Frames[0]
	min, max := 2<<16-1, 0
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			intensity := int(frame[x*2+y*w])<<8 + (int(frame[x*2+1+y*w]))
			if intensity > max {
				max = intensity
			}
			if intensity < min {
				min = intensity
			}
		}
	}

	rawData := make([]byte, w*h)
	maxMin := max - min + 1
	for x := 0; x < w; x++ {
		for y := 0; y < h; y++ {
			intensity := int(frame[x*2+2*y*w])<<8 + (int(frame[x*2+1+2*y*w]))
			newValue := int(float32(intensity*1.0) / float32((maxMin+1.0)/256.0))
			rawData[(x + y*h)] = byte(newValue)
		}
	}
	return rawData, w, h
}

func InitScheme() {
	conn := GetConfig().Database.GetConnection()
	_, err := conn.Exec(SchemeQ)
	if err != nil {
		panic(err.Error())
	}
}

func NewDate(day int, month int, year int) time.Time {
	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC)
}
