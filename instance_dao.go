package main

var instanceDaoInstance InstanceDao

// Instances
type InstanceDao interface {
	Save(instance Instance) (Instance, error)
	Find(id int) (Instance, error)
	FindAll() []Study
	Delete(id int) bool
	Exists(id int) bool
}

func GetInstanceDao() InstanceDao {
	if instanceDaoInstance == nil {
		instanceDaoInstance = &instanceDao{}
	}
	return instanceDaoInstance
}

type instanceDao struct {
}

func (*instanceDao) Save(instance Instance) (Instance, error) {
	panic("implement me")
}

func (*instanceDao) Find(id int) (Instance, error) {
	panic("implement me")
}

func (*instanceDao) FindAll() []Study {
	panic("implement me")
}

func (*instanceDao) Delete(id int) bool {
	panic("implement me")
}

func (*instanceDao) Exists(id int) bool {
	panic("implement me")
}
